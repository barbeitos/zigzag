<?php

namespace AppBundle\Controller;

use AppBundle\Services\ReaderServices;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ZigZagController extends Controller
{
    /**
     * Root method that will show the list of repositories
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $reader = new ReaderServices();

        return $this->render('@App/list.html.twig', array(
            'data' => $reader->getData()
        ));
    }

    /**
     * Method to export the list of repositories in a CVS document
     *
     * @Route("/export", name="export")
     */
    public function exportAction(Request $request){
        $response = new StreamedResponse();

        $response->setCallback(function() {
            $reader = new ReaderServices();
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, array('Name', 'Owner', 'Image', 'Stars', 'Forks', 'Open issues'),';');

            $results = $reader->getData();

            foreach($results as $row) {
                fputcsv(
                    $handle,
                    array(
                        $row['name'],
                        $row['owner']['login'],
                        $row['owner']['avatar_url'],
                        $row['stargazers_count'],
                        $row['forks_count'],
                        $row['open_issues_count']
                    ),
                    ';'
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

}

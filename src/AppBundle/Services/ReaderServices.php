<?php

namespace AppBundle\Services;

use http\Exception\InvalidArgumentException;

class ReaderServices
{
    /**
     * @var string $baseUrl
     */
    private $baseUrl = 'https://api.github.com/orgs/symfony/repos';

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }


    /**
     * Method to clean URL
     *
     * @param $url
     * @return string
     * @throws InvalidArgumentException
     */
    private static function cleanUrl($url){
        if(is_null($url) || !is_string($url)) {
            throw new InvalidArgumentException('Invalid Url.');
        }

        $matchCount = preg_match("#^(https?://[^/]+)#", $url, $matches);
        if ($matchCount == 0) {
            throw new InvalidArgumentException('Invalid Url format.');
        }

        $protocol = $matches[1];

        $query = substr($url, strlen($protocol));
        $query = preg_replace("#//+#", "/", $query);

        return $protocol.$query;
    }

    /**
     * Method for obtaining data from Symfony repositories
     *
     * @return mixed
     */
    public function getData(){
        $queryBuilder = $this->getBaseUrl();
        $queryUrl = $this->cleanUrl($queryBuilder);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $queryUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'User-Agent: Awesome-Octocat-App'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response, true);
    }
}